const Express =require('express');
const Router = Express.Router();
const {Funcion} =require('../Node-rsa/Funciones');
const Saludo = require('../Routes/Prueba');

const Nombre = 'Alex';

Router.get('', async(req, res)=>{

  res.render('Mensajeria', {Funciones:Funcion, Nombre:Nombre, Saludo:Saludo});

});

module.exports = Router;