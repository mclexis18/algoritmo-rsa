const {PORT} =require('./Config/Index');
const Express = require('express');
const App = Express();
const Path = require('path');

const {Mensajeria} = require('./Routes/Index');

App.set('views', './views');
App.set('view engine', 'pug');
App.set(Express.json());
App.set(Express.urlencoded({extended: false}))
App.set(Express.static(Path.join(__dirname, 'Public')));
App.set(Express.static(Path.join(__dirname, 'Keys')));

App.use('/Mensajeria', Mensajeria);

App.listen(PORT || 5000, ()=>{
    console.log('corremos')
});