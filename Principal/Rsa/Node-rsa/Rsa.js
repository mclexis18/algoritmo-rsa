const Rsa =require('node-rsa');
const Archivo = require('fs');

const Llaves = new Rsa().generateKeyPair();

const LlavePublica = Llaves.exportKey('public');
const LlavePrivada = Llaves.exportKey('private');

Archivo.writeFileSync('../Keys/Publica.pem', LlavePublica, 'utf8');
Archivo.writeFileSync('../Keys/Privada.pem', LlavePrivada, 'utf8');




