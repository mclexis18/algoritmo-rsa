const Rsa = require('node-rsa');
const Fs = require('fs');


const FuncionCriptar =(Mensaje)=>{

    let LlavePublica = new Rsa();
    let FilewithPublicKey = Fs.readFileSync('../Keys/Publica.pem', 'utf8');
    LlavePublica.importKey(FilewithPublicKey);
    let Encriptado = LlavePublica.encrypt(Mensaje, 'base64');
    console.log(Encriptado);

}

const FuncionDescrip =(Mensaje)=>{

    let LlavePrivada = new Rsa();
    const FilewithkeyPrivate = Fs.readFileSync('../Keys/Privada.pem', 'utf8');
    LlavePrivada.importKey(FilewithkeyPrivate);
    const Descript = LlavePrivada.decrypt(Mensaje, 'utf8');
    console.log(Descript)

}

const FuncionSaludar=()=>{

    console.log('Hola estoy aqui');

}

module.exports = {

    Encriptar:FuncionCriptar,
    Desencriptar:FuncionDescrip,
    Funcion:FuncionSaludar

}