const Rsa = require('node-rsa');
const FileRead = require('fs');

let PublicKey = new Rsa();
let PrivateKey = new Rsa();

let filewithPublicKey = FileRead.readFileSync('../Keys/PublicKey.pem', 'utf8');
let filewithPrivateKey = FileRead.readFileSync('../Keys/PrivateKey.pem', 'utf8');

PublicKey.importKey(filewithPublicKey);
PrivateKey.importKey(filewithPrivateKey);

const Data = 'Alex';

const MessageEncrypt = PublicKey.encrypt(Data, 'base64');
console.log(MessageEncrypt);
