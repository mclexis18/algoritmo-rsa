const Rsa = require('node-rsa');
const FileRead = require('fs');

let Keys = new Rsa().generateKeyPair();

let PrivateKey = Keys.exportKey('private');
let PublicKey = Keys.exportKey('public');

FileRead.writeFileSync('../Keys/PrivateKey.pem', PrivateKey, 'utf8');
FileRead.writeFileSync('../Keys/PublicKey.pem', PublicKey, 'utf8');
